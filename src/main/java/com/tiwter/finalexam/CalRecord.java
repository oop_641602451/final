/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tiwter.finalexam;

/**
 *
 * @author TUF
 */
public class CalRecord {
    private double firstnum;
    private double secondnum;
    private double result;
    private String operation;

    @Override
    public String toString() {
        return "CalRecord{"+ firstnum + operation + secondnum  +" = " +  result + '}';
    }

    public double getFirstnum() {
        return firstnum;
    }

    public void setFirstnum(double firstnum) {
        this.firstnum = firstnum;
    }

    public double getSecondnum() {
        return secondnum;
    }

    public void setSecondnum(double secondnum) {
        this.secondnum = secondnum;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public CalRecord(double firstnum, double secondnum, double result, String operation) {
        this.firstnum = firstnum;
        this.secondnum = secondnum;
        this.result = result;
        this.operation = operation;
    }
}
